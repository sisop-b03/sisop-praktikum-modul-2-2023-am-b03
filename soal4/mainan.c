#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

int main(int argc, char *argv[]) 
{
  pid_t pid, sid, pid2;
  time_t current_time;
  struct tm *current_time_info;
  int hour, minute, seconds;
  char *path;

  if (argc != 5) {
    printf("Argumen: %s hour minute seconds path\n", argv[0]);
    return 1;
  }

  char *hour_arg = argv[1];
  char *minute_arg = argv[2];
  char *seconds_arg = argv[3];
  path = argv[4];

  if (hour_arg[0] != '*')
  {
    if (hour_arg[1] == '\0')
    {
      if ((hour_arg[0] < '0') || (hour_arg[0] > '9'))
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
    }
    else
    {
      if (hour_arg[2] != '\0')
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
      if ((hour_arg[0] == '0') || (hour_arg[0] == '1'))
      {
        if ((hour_arg[1] < '0') || (hour_arg[1] > '9'))
        {
          printf("Error: Argumen jam invalid.\n");
          return 1;
        }
      }
      if (hour_arg[0] == '2')
      {
        if ((hour_arg[1] < '0') || (hour_arg[1] > '3'))
        {
          printf("Error: Argumen jam invalid.\n");
          return 1;
        }
      }
      if ((hour_arg[0] < '0') || (hour_arg[0] > '2'))
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
    }
  }

  if (minute_arg[0] != '*')
  {
    if (minute_arg[1] == '\0')
    {
      if ((minute_arg[0] < '0') || (minute_arg[0] > '9'))
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
    }
    else
    {
      if (minute_arg[2] != '\0')
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
      if ((minute_arg[0] >= '0') && (minute_arg[0] <= '5'))
      {
        if ((minute_arg[1] < '0') || (minute_arg[1] > '9'))
        {
          printf("Error: Argumen menit invalid.\n");
          return 1;
        }
      }
      if ((minute_arg[0] < '0') || (minute_arg[0] > '5'))
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
    }
  }

  if (seconds_arg[0] != '*')
  {
    if (seconds_arg[1] == '\0')
    {
      if ((seconds_arg[0] < '0') || (seconds_arg[0] > '9'))
      {
        printf("Error: Argumen detik invalid.\n");
        return 1;
      }
    }
    else
    {
      if (seconds_arg[2] != '\0')
      {
        printf("Error: Argumen detik invalid.\n");
        return 1;
      }
      if ((seconds_arg[0] >= '1') && (seconds_arg[0] <= '5'))
      {
        if ((seconds_arg[1] < '0') || (seconds_arg[1] > '9'))
        {
          printf("Error: Argumen detik invalid.\n");
          return 1;
        }
      }
      if (seconds_arg[0] > '5')
      {
        printf("Error: Argumen detik invalid.\n");
        return 1;
      }
    }
  }
  

  if (hour_arg[0] == '*') 
  {
    hour = -1;
  } 
  else 
  {
    hour = atoi(hour_arg);
  }

  if (minute_arg[0] == '*') 
  {
    minute = -1;
  } 
  else 
  {
    minute = atoi(minute_arg);
  }

  if (seconds_arg[0] == '*') 
  {
    seconds = -1;
  } 
  else 
  {
    seconds = atoi(seconds_arg);
  }

  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1) 
  {
    time_t now = time(NULL);
    struct tm *tm_now = localtime(&now);

    if ((hour == -1 || tm_now->tm_hour == hour) && (minute == -1 || tm_now->tm_min == minute) && (seconds == -1 || tm_now->tm_sec == seconds))
    {
      pid2 = fork();
      if (pid2 == 0)
      {
        char *args[] = { "sh", path, NULL };
        execvp("sh", args);
        exit(0);
      }
    }
    sleep(1);
  }
}
