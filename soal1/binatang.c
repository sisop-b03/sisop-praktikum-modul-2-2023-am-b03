#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

int main()
{
  pid_t pid1, pid2, pid3, pid4, pid5, pid6;
  int status1, status2, status3, status4, status5, status6;
  char src_path[100];
  char dst_path[100];
  int check;

  pid1 = fork();
  if (pid1 == 0)
  {
    execlp("wget", "wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL);
    exit(0);
  }
  waitpid(pid1, &status1, 0);

  pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAir", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid3 = fork();
  if (pid3 == 0)
  {
    execlp("unzip", "unzip", "binatang.zip", "-d", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1", NULL);
    exit(0);
  }
  waitpid(pid3, &status3, 0);

  DIR *dir;
  struct dirent *entry;
  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/1");
  int animal_count = 0;
  while ((entry = readdir(dir)) != NULL)
  {
    if (entry->d_type == DT_REG)
    {
      if (strstr(entry->d_name, "_") != NULL)
      {
        animal_count++;
      }
    }
  }

  srand(time(NULL));

  while(1)
  {
    rewinddir(dir);
    check = rand() % animal_count;
    int idx = 0;
    while ((entry = readdir(dir)) != NULL) 
    {
      if (entry->d_type == DT_REG)
      {
        if (idx != check)
        {
          idx++;
          continue;
        }
        if (strstr(entry->d_name, "_") == NULL)
        {
          continue;
        }
        animal_count--;

        sprintf(src_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/%s", entry->d_name);

        if (strstr(entry->d_name, "_darat") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanDarat/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }

        if (strstr(entry->d_name, "_air") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAir/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }

        if (strstr(entry->d_name, "_amphibi") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAmphibi/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }
        break;
      }
    }
    if (animal_count == 0)
    {
      break;
    }
  }
  closedir(dir);

  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);

  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanAir", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);

  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);
}
