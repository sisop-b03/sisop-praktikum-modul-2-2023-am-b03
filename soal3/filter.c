#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int cnt_bek, cnt_gelandang, cnt_penyerang, cnt_kiper;

typedef struct {
    char name[100];
    char position[100];
    int rating;
} Player;

int compare_players(const void *p1, const void *p2) {
    const Player *player1 = (const Player *) p1;
    const Player *player2 = (const Player *) p2;
    
    if (player1->rating < player2->rating) {
        return 1;
    }
    else if (player1->rating > player2->rating) {
        return -1;
    }
    else {
        return 0;
    }
}

void buatTim(int num_bek, int num_gelandang, int num_penyerang) {
  char *positions[] = {"Bek", "Gelandang", "Kiper", "Penyerang"};
  int num_players[] = {num_bek, num_gelandang, 1, num_penyerang};
  char *dirs[] = {"Bek", "Gelandang", "Kiper", "Penyerang"};
  Player players_bek[100], players_gelandang[100], players_penyerang[100], players_kiper[100];
  Player team[11];
  pid_t pid1;
  int status1;

  char file_name[PATH_MAX];
  sprintf(file_name, "/home/richard160421084/Formasi_%d-%d-%d.txt", num_bek, num_gelandang, num_penyerang);
  FILE *output_file = fopen(file_name, "w");
  if (output_file == NULL) {
    perror("Failed to open output file");
    return;
  }
  
  for (int i=1; i<=cnt_bek; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }
    struct dirent *entry;
    int player_count = 0;
    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) { 
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);

        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_bek[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }

  for (int i=1; i<=cnt_gelandang; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }

    struct dirent *entry;
    int player_count = 0;
    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) { 
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);

        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_gelandang[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }

  for (int i=1; i<=cnt_kiper; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }

    struct dirent *entry;
    int player_count = 0;

    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) { 
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);
        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_kiper[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }

  for (int i=1; i<=cnt_penyerang; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }

    struct dirent *entry;
    int player_count = 0;
    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) { 
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);
        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_penyerang[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }

  qsort(players_bek, cnt_bek, sizeof(Player), compare_players);
  qsort(players_gelandang, cnt_gelandang, sizeof(Player), compare_players);
  qsort(players_kiper, cnt_kiper, sizeof(Player), compare_players);
  qsort(players_penyerang, cnt_penyerang, sizeof(Player), compare_players);

  for (int i=0; i<num_bek; i++)
  {
    team[i] = players_bek[i];
  }

  for (int i=0; i<num_gelandang; i++)
  {
    team[num_bek+i] = players_gelandang[i];
  }

  for (int i=0; i<num_penyerang; i++)
  {
    team[num_bek+num_gelandang+i] = players_penyerang[i];
  }

  team[10] = players_kiper[0];

  fprintf(output_file, "Formasi Tim:\n");
  for (int i = 0; i < 11; i++) {
    fprintf(output_file, "%s_ManUtd_%s_%d\n", team[i].name, team[i].position, team[i].rating);
  }

  fclose(output_file);
}

int main() {
  pid_t pid1, pid2, pid3, pid4, pid5;
  int status1, status2, status3, status4, status5;

  pid1 = fork();
  if (pid1 == 0) {
    execlp("wget", "wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL);
    exit(0);
  }
  waitpid(pid1, &status1, 0);

  pid2 = fork();
  if (pid2 == 0) {
    execlp("unzip", "unzip", "players.zip", "-d", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid3 = fork();
  if (pid3 == 0) {
    execlp("rm", "rm", "players.zip", NULL);
    exit(0);
  }
  waitpid(pid3, &status3, 0);

  DIR *dir;
  struct dirent *entry;
  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;
      }
      if (strstr(entry->d_name, "_ManUtd_") == NULL) {
        char path[100];
        sprintf(path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players/%s", entry->d_name);
        pid4 = fork();
        if (pid4 == 0) {
          execlp("rm", "rm", path, NULL);
          exit(0);
        }
        waitpid(pid4, &status4, 0);
      }
    }
    closedir(dir);
  }

  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;
      }
      char src_path[100];
      char dst_path[100];
      sprintf(src_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players/%s", entry->d_name);
      if (strstr(entry->d_name, "_Kiper_") != NULL) {
        cnt_kiper++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Bek_") != NULL) {
        cnt_bek++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Bek_") != NULL) {
        cnt_bek++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Gelandang_") != NULL) {
        cnt_gelandang++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Penyerang_") != NULL) {
        cnt_penyerang++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      }
    }
    closedir(dir);
  }
  
  for (int i=1; i<=cnt_bek; i++)
  {
    for (int j=1; j<=cnt_gelandang; j++)
    {
      int k = 10-i-j;
      if ((k > 0) && (k<=cnt_penyerang))
      {
        buatTim(i,j,k);
      }
    }
  }
}
