# No 1

Soal ini meminta beberapa hal,

1. Mendownload binatang.zip dari `https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq`
2. Lakukan unzip pada file download tadi
3. Buat 3 folder dan namakan `HewanDarat`, `HewanAmphibi`, dan `HewanAir`
4. Pilih file gambar secara acak dari file yang telah di unzip
5. Pindah file yang terpilih ke folder yang baru sesuai tempat tinggalnya
6. Lakukan langkah (4) dan (5) hingga seluruh file telah terpindahkan
7. Setelah semua file dipindahkan, zip setiap folder dan hapus folder tersebut

Untuk menyelesaikan soal, ini lakukan langkah sebagai berikut

1. Buat file bernama `binatang.c` dengan menggunakan command `nano binatang.c`
   ![buat file program](images/1_1.jpeg)
2. Tambahkan code berikut ke file `binatang.c` tersebut

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

int main()
{
  pid_t pid1, pid2, pid3, pid4, pid5, pid6;
  int status1, status2, status3, status4, status5, status6;
  char src_path[100];
  char dst_path[100];
  int check;

  pid1 = fork();
  if (pid1 == 0)
  {
    execlp("wget", "wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL);
    exit(0);
  }
  waitpid(pid1, &status1, 0);

  pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAir", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid3 = fork();
  if (pid3 == 0)
  {
    execlp("unzip", "unzip", "binatang.zip", "-d", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1", NULL);
    exit(0);
  }
  waitpid(pid3, &status3, 0);

  DIR *dir;
  struct dirent *entry;
  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/1");
  int animal_count = 0;
  while ((entry = readdir(dir)) != NULL)
  {
    if (entry->d_type == DT_REG)
    {
      if (strstr(entry->d_name, "_") != NULL)
      {
        animal_count++;
      }
    }
  }

  srand(time(NULL));

  while(1)
  {
    rewinddir(dir);
    check = rand() % animal_count;
    int idx = 0;
    while ((entry = readdir(dir)) != NULL)
    {
      if (entry->d_type == DT_REG)
      {
        if (idx != check)
        {
          idx++;
          continue;
        }
        if (strstr(entry->d_name, "_") == NULL)
        {
          continue;
        }
        animal_count--;

        sprintf(src_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/%s", entry->d_name);

        if (strstr(entry->d_name, "_darat") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanDarat/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }

        if (strstr(entry->d_name, "_air") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAir/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }

        if (strstr(entry->d_name, "_amphibi") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAmphibi/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }
        break;
      }
    }
    if (animal_count == 0)
    {
      break;
    }
  }
  closedir(dir);

  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);

  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanAir", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);

  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);
}
```

3. Compile file program tadi dengan menggunakan command `gcc -o <hasil compile> <nama program>`, untuk soal ini digunakan nama compile `binatang`, maka gunakan command `gcc -o binatang binatang.c`  
   ![compile file program](images/1_2.jpeg)
4. Jalankan program dengan menggunakan command `./<hasil compile>`, maka disini digunakan command `./binatang`
   ![run file program](images/1_3.jpeg)  
   ![hasil run program](images/1_4.jpeg)

### Hasil program

a) HewanDarat.zip  
![isi Hewan Darat](images/1_5.jpeg)  
b) HewanAmphibi.zip  
![isi Hewan Amphibi](images/1_6.jpeg)  
c) HewanAir.zip  
![isi Hewan Air](images/1_7.jpeg)

### Penjelasan code

Pada hampir seluruh iterasi akan dilakukan forking sebab akan digunakan command exec(), apabila tidak dilakukan forking maka program akan berpindah ke program untuk menjalankan exec() dan tidak akan kembali ke program saat ini

```c
  pid1 = fork();
  if (pid1 == 0)
  {
    execlp("wget", "wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL);
    exit(0);
  }
  waitpid(pid1, &status1, 0);
```

- Melakukan download dari link yang diberikan kemudian menamakannya binatang.zip
- Untuk mendownload, urutan argumennya adalah sebagai berikut "wget", "wget", [link download], "-O", [nama file], NULL

```c
pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAir", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid2 = fork();
  if (pid2 == 0)
  {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

```

- Membuat 3 folder yang nantinya akan menjadi tujuan pemindahan file
- Untuk membuat folder, urutan argumennya adalah sebagai berikut "mkdir", "mkdir", [path ke folder], NULL

```c
  pid3 = fork();
  if (pid3 == 0)
  {
    execlp("unzip", "unzip", "binatang.zip", "-d", "/home/richard160421084/Documents/Sisop/Prak2/Shift/1", NULL);
    exit(0);
  }
  waitpid(pid3, &status3, 0);
```

- Melakukan unzip terhadap file bernama `binatang.zip` dan hasil unzip ditaruh pada folder `/home/richard160421084/Documents/Sisop/Prak2/Shift/1`
- Urutan argumen untuk melakukan unzip adalah "unzip", "unzip", [nama zip], "-d", [path untuk menaruh isi zip], NULL

```c
  DIR *dir;
  struct dirent *entry;
  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/1");
  int animal_count = 0;
  while ((entry = readdir(dir)) != NULL)
  {
    if (entry->d_type == DT_REG)
    {
      if (strstr(entry->d_name, "_") != NULL)
      {
        animal_count++;
      }
    }
  }
```

- Mendapatkan jumlah file yang nantinya akan dipindahkan dengan mengiterasi pada folder `/home/richard160421084/Documents/Sisop/Prak2/Shift/1`
- Nilai variabel `animal_count` hanya akan bertambah jika file tersebut memiliki karakter underscore (\_), sebab pada folder tersebut juga terdapat file lain yaitu file program, file compile dan binatang.zip

```c
  srand(time(NULL));
```

Melakukan inisialisasi untuk mendapatkan bilangan random nantinya

```c
  while(1)
  {
    rewinddir(dir);
    check = rand() % animal_count;
    int idx = 0;
    while ((entry = readdir(dir)) != NULL)
    {
      if (entry->d_type == DT_REG)
      {
        if (idx != check)
        {
          idx++;
          continue;
        }
        if (strstr(entry->d_name, "_") == NULL)
        {
          continue;
        }
        animal_count--;

        sprintf(src_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/%s", entry->d_name);

        if (strstr(entry->d_name, "_darat") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanDarat/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }

        if (strstr(entry->d_name, "_air") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAir/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }

        if (strstr(entry->d_name, "_amphibi") != NULL)
        {
          sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/1/HewanAmphibi/%s", entry->d_name);
          pid4 = fork();
          if (pid4 == 0)
          {
            execlp("mv", "mv", src_path, dst_path, NULL);
            exit(0);
          }
          waitpid(pid4, &status4, 0);
        }
        break;
      }
    }
    if (animal_count == 0)
    {
      break;
    }
  }
```

- Lakukan looping hingga didapat bahwa variabel `animal_count` bernilai 0, hal ini menandakan bahwa semua file hasil unzip telah dipindahkan ke 3 folder sesuai tempat tinggalnya
- Pada setiap loop, reset pointer iterasi folder kemudian dapatkan nilai bilangan random untuk menentukan index file yang akan dipindahkan apabila file tersebut merupakan file yang perlu dipindahkan dengan command `check = rand() % animal_count`
- Lakukan iterasi pada folder tersebut hingga index yang didapat dari hasil bilangan random sebelumnya, cek apakah nama file tersebut mengandung karakter underscore (\_), jika tidak maka artinya file ini tidak termasuk dalam file yang perlu dipindahkan
- Apabila ternyata file ini merupakan file yang perlu dipindahkan maka kurangkan nilai variabel `animal_count` dan dapatkan absolute path file tersebut
- Cek apakah hewan itu merupakan hewan darat, air atau amphibi dengan mengecek apakah mengandung substring "\_darat", "\_air", atau "\_amphibi" secara berurutan. Pindahkan file hewan tadi ke folder yang bersangkutan, kemudian keluarlah dari looping iterasi folder
- Urutan argumen untuk pemindahan file adalah sebagai berikut "mv", "mv", [path awal], [path akhir], NULL

```c
  closedir(dir);
```

Tutup folder setelah semua pemindahan dilakukan

```c
  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  pid5 = fork();
  if (pid5 == 0)
  {
    execlp("zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
```

- Zip ketiga folder yang telah dibuat
- Urutan argumen untuk melakukan operasi zip adalah "zip", "zip", "-r", [nama zip], [nama folder], NULL

```c
  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanDarat", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);

  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanAir", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);

  pid6 = fork();
  if (pid6 == 0)
  {
    execlp("rm", "rm", "-rf", "HewanAmphibi", NULL);
    exit(0);
  }
  waitpid(pid6, &status6, 0);
```

- Menghapus folder HewanDarat, HewanAir, HewanAmphibi
- Urutan argumen untuk menghapus folder adalah "rm", "rm", "-rf", [nama folder], NULL

### Catatan Tambahan

Karena pada path kami menggunakan absolute path maka ubahlah path sesuai dengan user yang sedang digunakan

### Kendala

Tidak terdapat kendala dalam pengerjaan soal ini

# No 2

### Permintaan Soal

a. Buat folder khusus dan program C yang setiap 30 detik membuat folder baru dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

b. Setelah folder dibuat, tiap folder diisi dengan 15 gambar yang di-download dari [https://picsum.photos/](https://picsum.photos/) , dimana tiap gambar di-download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

c. Setelah sebuah folder telah terisi oleh 15 gambar, folder akan di-zip dan folder akan di-delete (sehingga hanya menyisakan .zip).

d. Program akan men-generate sebuah program "killer" yang siap di-run(executable) untuk menterminasi semua operasi program tersebut. Setelah di-run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

e. Program utama bisa di-run dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, ter-zip lalu di-delete).

Catatan :

- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

### Penjelasan Hasil Program

Hasil Program terdiri dari beberapa fungsi yang terdiri dari

1. **Fungsi Daemon**

   Fungsi daemon ini digunakan untuk menjalankan program secara daemon, sehingga program akan berjalan di background dan tidak akan terhenti sampai program killer dijalankan

   ```c
   pid_t pid, sid;

   pid = fork();
   if(pid < 0) exit(EXIT_FAILURE);
   if(pid > 0) exit(EXIT_SUCCESS);

   umask(0);

   sid = setsid();
   if(sid < 0) exit(EXIT_FAILURE);

   createKiller(argv, pid, sid);
   close(STDIN_FILENO);
   close(STDOUT_FILENO);
   close(STDERR_FILENO);
   ```

   - Membuat proses parent menjadi child process dengan memanggil fungsi fork() `pid = fork();`. Jika fork() gagal, maka program akan keluar dengan status EXIT_FAILURE. Jika berhasil, maka program parent akan keluar dengan status EXIT_SUCCESS.
   - Mengatur file mode creation mask (umask) menjadi 0 dengan perintah `umask(0);`. Hal ini berguna agar file yang dihasilkan dapat diakses oleh semua user pada sistem.
   - Mengatur session id (sid) dari child process dengan perintah `sid = setsid();`. Hal ini berguna agar child process dapat berjalan secara mandiri tanpa memerlukan proses parent.
   - Menutup file descriptor standar (stdin, stdout, stderr) `close(STDIN_FILENO);` `close(STDOUT_FILENO);` `close(STDERR_FILENO);`. Hal ini agar program tidak terhubung dengan terminal.

2. **Fungsi Killer**

   Fungsi killer ini digunakan untuk membuat program killer yang akan digunakan untuk menghentikan program utama

   ```c
   void createKiller(char *argv[], pid_t pid, pid_t sid)
   {
     FILE *fileptr = fopen("killer.c", "w");

     // Killer.c
     char input[1024] = ""
     "#include <unistd.h>\n"
     "#include <wait.h>\n"
     "int main() {\n"
         "pid_t child = fork();\n"
         "if (child == 0) {\n"
             "%s"
             "execv(\"%s\", argv);\n"
         "}\n"
         "while(wait(NULL) > 0);\n"
         "char *argv[] = {\"rm\", \"killer\", NULL};\n"
         "execv(\"/bin/rm\", argv);\n"
     "}\n";

     // Mode A
     char command[1024];
     if (strcmp(argv[1], "-a") == 0) {
         sprintf(command, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
         fprintf(fileptr, input, command, "/usr/bin/pkill");
     }

     // Mode B
     if (strcmp(argv[1], "-b") == 0) {
         sprintf(command, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
         fprintf(fileptr, input, command, "/bin/kill");
     }

     fclose(fileptr);

     // Compile killer.c
     pid = fork();
     if(pid == 0)
     {
         char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
         execv("/usr/bin/gcc", command);
     }
     while(wait(NULL) != pid);

     // Remove killer.c
     pid = fork();
     if (pid == 0) {
       char *argv[] = {"rm", "killer.c", NULL};
       execv("/bin/rm", argv);
     }
     while(wait(NULL) != pid);
   }
   ```

   dari proses tersebut akan dihasilkan program c killer.c seperti berikut

   ```c
    #include <unistd.h>
    #include <wait.h>
    int main() {
        pid_t child = fork();
        if (child == 0) {
            //Mode A
            char *argv[] = {"pkill", "-9", "-s", [sid], NULL};
            execv("/usr/bin/pkill", argv);

            //Mode B
            char *argv[] = {"kill", "-9", [pid], NULL};
            execv("/bin/kill", argv);
        }
        while(wait(NULL) > 0);
        char *argv[] = {"rm", "killer", NULL};
        execv("/bin/rm", argv);
    }
   ```

   - Membuat file killer.c dengan mode write `FILE *fileptr = fopen("killer.c", "w");`
   - Menuliskan kode-kode C yang telah diatur dalam variabel input ke dalam file killer.c
   - Mengganti argumen pada kode-kode C yang sudah ditulis dengan nilai-nilai yang telah ditentukan sesuai mode yang dipilih
   - Menutup file killer.c `fclose(fileptr);`
   - Membuat program killer dengan cara meng-compile file killer.c yang sudah dibuat `char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};`
   - Menghapus file killer.c `char *argv[] = {"rm", "killer.c", NULL};`
   - Pada fungsi createKiller ini terdapat dua mode yang dapat dipilih, yaitu mode A dan mode B
     - Mode A digunakan untuk menghentikan semua proses yang berjalan pada program utama menggunakan perintah `pkill -9 -s [sid]` sid akan diisi dengan nilai SID dari daemon
     - Mode B digunakan untuk menghentikan proses program utama menggunakan perintah `kill -9 [pid]` pid akan diisi dengan nilai PID dari program utama, sehingga proses child akan diselesaikan terlebih dahulu sebelum dihentikan

3. **Fungsi Membuat Folder**

   Fungsi Membuat Folder digunakan untuk membuat folder baru dengan nama timestamp [YYYY-MM-dd_HH:mm:ss]. Proses pembuatan folder dilakukan setiap 30 detik.

   ```c
   void createFolder(char* folderName)
   {
       pid_t child;
       child = fork();
       if(child == 0)
       {
           char *command[] = {"mkdir", folderName, NULL};
           execv("/bin/mkdir", command);
       }
       while(wait(NULL) != child);
   }
   ```

   - Fungsi ini untuk folder baru dengan nama timestamp [YYYY-MM-dd_HH:mm:ss] dengan perintah `char *command[] = {"mkdir", folderName, NULL};`

4. **Fungsi Download Gambar**

   Fungsi Download Gambar digunakan untuk mendownload 15 gambar dari website https://picsum.photos/ dengan interval 5 detik setiap gambar. Setiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss]. Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping).

   ```c
   void downloadImage(char* folderName)
   {
       pid_t child;
       char imgName[30], location[50], link[50];

       for(int i = 0; i < 15; i++)
       {
           child = fork();
           if(child == 0)
           {
               time_t t = time(NULL);
               struct tm* localTime = localtime(&t);
               strftime(imgName, 30, "%Y-%m-%d_%H:%M:%S.png", localTime);
               sprintf(location, "%s/%s", folderName, imgName);
               sprintf(link, "https://picsum.photos/%ld", (t % 1000) + 50);

               char *command[] = {"wget", "-q", "-O", location, link, NULL};
               execv("/usr/bin/wget", command);
           }
           sleep(5);
       }
       while(wait(NULL) > 0);
   }
   ```

   - Fungsi ini untuk mendownload 15 gambar dari website https://picsum.photos/ dengan perintah `char *command[] = {"wget", "-q", "-O", location, link, NULL};`
   - Gambar akan diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].png dengan perintah `strftime(imgName, 30, "%Y-%m-%d_%H:%M:%S.png", localTime);`
   - Fungsi ini akan berjalan interval 5 detik setiap gambar dengan perintah `sleep(5);`
   - Fungsi ini akan dijalankan secara bersamaan (overlapping) dengan fungsi lainnya dengan perintah `while(wait(NULL) > 0);`

5. **Fungsi Zip Folder**

   Fungsi Zip Folder digunakan untuk melakukan kompresi pada folder yang telah terisi 15 gambar dengan format zip. Fungsi ini dijalankan setelah folder berhasil terisi semua gambar.

   ```c
   void zipFolder(char* folderName)
   {
       char zipName[30];
       sprintf(zipName, "%s.zip", folderName);

       pid_t child;
       child = fork();
       if(child == 0)
       {
           char *command[] = {"zip", "-r", zipName, folderName, NULL};
           execv("/usr/bin/zip", command);
       }
       while(wait(NULL) != child);
   }
   ```

   - Fungsi ini untuk melakukan kompresi pada folder yang telah terisi 15 gambar dengan format zip dengan perintah `char *command[] = {"zip", "-r", zipName, folderName, NULL};`
   - Fungsi ini akan dijalankan setelah folder berhasil terisi semua gambar dengan perintah `while(wait(NULL) != child);`

6. **Fungsi Delete Folder**

   Fungsi Delete Folder digunakan untuk menghapus folder yang telah dikompresi menjadi zip. Fungsi ini dijalankan setelah folder berhasil dikompresi menjadi zip.

   ```c
   void removeFolder(char* folderName)
   {
       char *command[] = {"rm", "-r", folderName, NULL};
       execv("/bin/rm", command);
   }
   ```

   - Fungsi ini untuk menghapus folder yang telah dikompresi menjadi zip dengan perintah `char *command[] = {"rm", "-r", folderName, NULL};`

### Screenshoot Hasil Program

1. **Hasil Proses createFolder dan zipping**  
   ![Hasil Proses createFolder dan zipping](images/2_1.jpg)

2. **Hasil Proses downloadImage**  
   ![Hasil Proses downloadImage](images/2_2.jpg)

3. **Hasil Proses Zip Images**  
   ![Hasil Proses Zip Images](images/2_3.jpg)

4. **Hasil Proses Killer Mode A**  
   ![Hasil Proses Killer Mode A](images/2_4.jpg)

   - Proses Killer A berhenti secara langsung.

5. **Hasil Proses Killer Mode B**  
   ![Hasil Proses Killer Mode B](images/2_5.jpg)

   - Proses Killer B berhenti ketika semua file sudah terkompresi menjadi zip.

6. **Hasil Overlapping Download Images**  
   ![Hasil Overlapping Download Images](images/2_6.jpg)
   ![Hasil Overlapping Download Images](images/2_7.jpg)
   - Hasil overlapping download images diatas dapat dilihat dari nama file images yang terdownload di waktu yang sama dari 2 folder yang berbeda yg dimulai dari `19:08:21 - 19:09:01`.

### Kendala

- sempat terkendala pada proses overlapping namun dapat teratasi dengan menambahkan perintah `while(wait(NULL) > 0);` pada fungsi downloadImage.

# No 3

Soal ini meminta beberapa hal,

1. Download players.zip dari `https://drive.google.com/file/d/1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF/view?usp=share_link`
2. Lakukan unzip pada players.zip
3. Hapus file players.zip
4. Hapus semua pemain yang bukan dalam tim Manchester United
5. Kategorikan file sisanya menjadi 4 folder yaitu Kiper, Bek, Gelandang dan Penyerang
6. Buat tim kesebelasan dari pemain yang tersisa. Setiap tim memiliki minimal 1 Bek, minimal 1 Gelandang, minimal 1 Penyerang, dan tepat 1 Kiper. Tulis tim tersebut dalam file bernama `Formasi_[jumlah Bek]-[jumlah Gelandang]-[jumlah Penyerang].txt` dan taruh di `/home/[users]`

Untuk menyelesaikan soal tersebut dilakukan langkah sebagai berikut

1. Buat file bernama `filter.c` dengan menggunakan command `nano filter.c`  
   ![buat file program](images/3_1.jpeg)
2. Tambahkan code berikut ke file `filter.c` tersebut

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int cnt_bek, cnt_gelandang, cnt_penyerang, cnt_kiper;

typedef struct {
    char name[100];
    char position[100];
    int rating;
} Player;

int compare_players(const void *p1, const void *p2) {
    const Player *player1 = (const Player *) p1;
    const Player *player2 = (const Player *) p2;

    if (player1->rating < player2->rating) {
        return 1;
    }
    else if (player1->rating > player2->rating) {
        return -1;
    }
    else {
        return 0;
    }
}

void buatTim(int num_bek, int num_gelandang, int num_penyerang) {
  char *positions[] = {"Bek", "Gelandang", "Kiper", "Penyerang"};
  int num_players[] = {num_bek, num_gelandang, 1, num_penyerang};
  char *dirs[] = {"Bek", "Gelandang", "Kiper", "Penyerang"};
  Player players_bek[100], players_gelandang[100], players_penyerang[100], players_kiper[100];
  Player team[11];
  pid_t pid1;
  int status1;

  char file_name[PATH_MAX];
  sprintf(file_name, "/home/richard160421084/Formasi_%d-%d-%d.txt", num_bek, num_gelandang, num_penyerang);
  FILE *output_file = fopen(file_name, "w");
  if (output_file == NULL) {
    perror("Failed to open output file");
    return;
  }

  for (int i=1; i<=cnt_bek; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }
    struct dirent *entry;
    int player_count = 0;
    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) {
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);

        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_bek[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }

  for (int i=1; i<=cnt_gelandang; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }

    struct dirent *entry;
    int player_count = 0;
    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) {
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);

        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_gelandang[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }

  for (int i=1; i<=cnt_kiper; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }

    struct dirent *entry;
    int player_count = 0;

    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) {
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);
        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_kiper[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }

  for (int i=1; i<=cnt_penyerang; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }

    struct dirent *entry;
    int player_count = 0;
    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) {
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);
        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_penyerang[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }

  qsort(players_bek, cnt_bek, sizeof(Player), compare_players);
  qsort(players_gelandang, cnt_gelandang, sizeof(Player), compare_players);
  qsort(players_kiper, cnt_kiper, sizeof(Player), compare_players);
  qsort(players_penyerang, cnt_penyerang, sizeof(Player), compare_players);

  for (int i=0; i<num_bek; i++)
  {
    team[i] = players_bek[i];
  }

  for (int i=0; i<num_gelandang; i++)
  {
    team[num_bek+i] = players_gelandang[i];
  }

  for (int i=0; i<num_penyerang; i++)
  {
    team[num_bek+num_gelandang+i] = players_penyerang[i];
  }

  team[10] = players_kiper[0];

  fprintf(output_file, "Formasi Tim:\n");
  for (int i = 0; i < 11; i++) {
    fprintf(output_file, "%s_ManUtd_%s_%d\n", team[i].name, team[i].position, team[i].rating);
  }

  fclose(output_file);
}

int main() {
  pid_t pid1, pid2, pid3, pid4, pid5;
  int status1, status2, status3, status4, status5;

  pid1 = fork();
  if (pid1 == 0) {
    execlp("wget", "wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL);
    exit(0);
  }
  waitpid(pid1, &status1, 0);

  pid2 = fork();
  if (pid2 == 0) {
    execlp("unzip", "unzip", "players.zip", "-d", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);

  pid3 = fork();
  if (pid3 == 0) {
    execlp("rm", "rm", "players.zip", NULL);
    exit(0);
  }
  waitpid(pid3, &status3, 0);

  DIR *dir;
  struct dirent *entry;
  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;
      }
      if (strstr(entry->d_name, "_ManUtd_") == NULL) {
        char path[100];
        sprintf(path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players/%s", entry->d_name);
        pid4 = fork();
        if (pid4 == 0) {
          execlp("rm", "rm", path, NULL);
          exit(0);
        }
        waitpid(pid4, &status4, 0);
      }
    }
    closedir(dir);
  }

  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);

  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;
      }
      char src_path[100];
      char dst_path[100];
      sprintf(src_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players/%s", entry->d_name);
      if (strstr(entry->d_name, "_Kiper_") != NULL) {
        cnt_kiper++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Bek_") != NULL) {
        cnt_bek++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Bek_") != NULL) {
        cnt_bek++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Gelandang_") != NULL) {
        cnt_gelandang++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Penyerang_") != NULL) {
        cnt_penyerang++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      }
    }
    closedir(dir);
  }

  for (int i=1; i<=cnt_bek; i++)
  {
    for (int j=1; j<=cnt_gelandang; j++)
    {
      int k = 10-i-j;
      if ((k > 0) && (k<=cnt_penyerang))
      {
        buatTim(i,j,k);
      }
    }
  }
}
```

3. Compile file program tadi dengan menggunakan command `gcc -o <hasil compile> <nama program>`, untuk soal ini digunakan nama compile `filter`, maka gunakan command `gcc -o filter filter.c`  
   ![compile file program](images/3_2.jpeg)
4. Jalankan program dengan menggunakan command `./<hasil compile>`, maka disini digunakan command `./filter`
   ![run file program](images/3_3.jpeg)  
   ![hasil run program](images/3_4.jpeg)

### Hasil program

a) Bek  
![isi folder Bek](images/3_5.jpeg)  
b) Gelandang  
![isi folder Gelandang](images/3_6.jpeg)  
c) Penyerang  
![isi folder Penyerang](images/3_7.jpeg)  
d) Kiper  
![isi folder Kiper](images/3_8.jpeg)  
e) Daftar Formasi  
![Daftar Formasi](images/3_9.jpeg)  
f) Contoh Formasi  
![Contoh Formasi 1](images/3_10.jpeg)  
![Contoh Formasi 2](images/3_11.jpeg)

### Penjelasan code

Pada hampir seluruh iterasi akan dilakukan forking sebab akan digunakan command exec(), apabila tidak dilakukan forking maka program akan berpindah ke program untuk menjalankan exec() dan tidak akan kembali ke program saat ini

```c
int cnt_bek, cnt_gelandang, cnt_penyerang, cnt_kiper;
```

Variabel global yang menandakan jumlah total bek, gelandang, penyerang, dan kiper secara berurutan

```c
typedef struct {
    char name[100];
    char position[100];
    int rating;
} Player;
```

Disini didefinisikan suatu struct/class bernama player dengan field name, position, dan rating

```c
int compare_players(const void *p1, const void *p2) {
    const Player *player1 = (const Player *) p1;
    const Player *player2 = (const Player *) p2;

    if (player1->rating < player2->rating) {
        return 1;
    }
    else if (player1->rating > player2->rating) {
        return -1;
    }
    else {
        return 0;
    }
}
```

Ini merupakan fungsi yang akan digunakan untuk mengurutkan rating pemain dari besar ke kecil

```c
  sprintf(file_name, "/home/richard160421084/Formasi_%d-%d-%d.txt", num_bek, num_gelandang, num_penyerang);
  FILE *output_file = fopen(file_name, "w");
  if (output_file == NULL) {
    perror("Failed to open output file");
    return;
  }
```

Disini didefinisikan untuk file output dari formasi tim

```c
  for (int i=1; i<=cnt_bek; i++)
  {
    char dir_path[PATH_MAX];
    sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek");
    DIR *dirp = opendir(dir_path);
    if (dirp == NULL) {
      perror("Failed to open directory");
      fclose(output_file);
      return;
    }
    struct dirent *entry;
    int player_count = 0;
    while ((entry = readdir(dirp)) != NULL) {
      if (entry->d_type == DT_REG) {
        char filename[100], name[100], team[100], position[100];
        int rating;
        sscanf(entry->d_name, "%[^_]_%[^_]_%[^_]_%d.png", name, team, position, &rating);

        Player player;
        strcpy(player.name, name);
        strcpy(player.position, position);
        player.rating = rating;
        players_bek[player_count] = player;
        player_count++;
      }
    }
    closedir(dirp);
  }
```

- Tambahkan semua pemain bek yang ada di folder `/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek` ke array pemain_bek[]
- Lakukan hal yang sama untuk pemain Gelandang, Penyerang, dan Kiper dengan mengubah nilai variabel `dir_path` sebagai berikut  
  `sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang");`  
  `sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang");`  
  `sprintf(dir_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper");`

```c
  qsort(players_bek, cnt_bek, sizeof(Player), compare_players);
  qsort(players_gelandang, cnt_gelandang, sizeof(Player), compare_players);
  qsort(players_kiper, cnt_kiper, sizeof(Player), compare_players);
  qsort(players_penyerang, cnt_penyerang, sizeof(Player), compare_players);
```

- Lakukan sorting pada array daftar pemain tadi
- Argumen untuk sorting adalah sebagai berikut <nama array>, <jumlah isi array>, <size class isi array>, <fungsi sorting>

```c
  for (int i=0; i<num_bek; i++)
  {
    team[i] = players_bek[i];
  }

  for (int i=0; i<num_gelandang; i++)
  {
    team[num_bek+i] = players_gelandang[i];
  }

  for (int i=0; i<num_penyerang; i++)
  {
    team[num_bek+num_gelandang+i] = players_penyerang[i];
  }

  team[10] = players_kiper[0];
```

Tambahkan pemain-pemain teratas dari setiap posisi ke array team[], dengan jumlah pemain setiap posisi bergantung pada penentuan awal untuk jumlah setiap posisinya

```c
  fprintf(output_file, "Formasi Tim:\n");
  for (int i = 0; i < 11; i++) {
    fprintf(output_file, "%s_ManUtd_%s_%d\n", team[i].name, team[i].position, team[i].rating);
  }

  fclose(output_file);
```

Outputkan semua isi array team[] ke file yang sebelumnya telah ditentukan sesuai dengan format yang telah ditentukan. Ketika sudah selesai, tutup file outputnya

```c
  pid1 = fork();
  if (pid1 == 0) {
    execlp("wget", "wget", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", "-O", "players.zip", NULL);
    exit(0);
  }
  waitpid(pid1, &status1, 0);
```

- Melakukan download dari link yang diberikan kemudian menamakannya players.zip
- Untuk mendownload, urutan argumennya adalah sebagai berikut "wget", "wget", [link download] , "-O", [nama flile] , NULL

```c
  pid2 = fork();
  if (pid2 == 0) {
    execlp("unzip", "unzip", "players.zip", "-d", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3", NULL);
    exit(0);
  }
  waitpid(pid2, &status2, 0);
```

- Melakukan unzip terhadap file bernama `players.zip` dan hasil unzip ditaruh di folder `/home/richard160421084/Documents/Sisop/Prak2/Shift/3`
- Urutan argumen untuk melakukan unzip adalah "unzip", "unzip", [nama zip] , "-d", [path untuk menaruh isi zip] , NULL

```c
  pid3 = fork();
  if (pid3 == 0) {
    execlp("rm", "rm", "players.zip", NULL);
    exit(0);
  }
  waitpid(pid3, &status3, 0);
```

- Menghapus file `players.zip`
- Urutan argumen untuk melakukan penghapusan file adalah "rm", "rm", [nama file], NULL

```c
  DIR *dir;
  struct dirent *entry;
  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;
      }
      if (strstr(entry->d_name, "_ManUtd_") == NULL) {
        char path[100];
        sprintf(path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players/%s", entry->d_name);
        pid4 = fork();
        if (pid4 == 0) {
          execlp("rm", "rm", path, NULL);
          exit(0);
        }
        waitpid(pid4, &status4, 0);
      }
    }
    closedir(dir);
  }
```

- Melakukan penghapusan semua file pada folder `/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players` yang nama filenya tidak mengandung substring "_ManUtd_", sebab artinya pemain tersebut bukan dari Machester United
- Setelah semua penghapusan selesai, tutup folder yang sedang dibuka

```c
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
  pid5 = fork();
  if (pid5 == 0) {
    execlp("mkdir", "mkdir", "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang", NULL);
    exit(0);
  }
  waitpid(pid5, &status5, 0);
```

- Buat folder Kiper, Bek, Gelandang, dan Penyerang
- Untuk mebuat folder, urutan argumennya adalah "mkdir", "mkdir", [path ke folder], NULL

```c
  dir = opendir("/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;
      }
      char src_path[100];
      char dst_path[100];
      sprintf(src_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/players/%s", entry->d_name);
      if (strstr(entry->d_name, "_Kiper_") != NULL) {
        cnt_kiper++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Kiper/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Bek_") != NULL) {
        cnt_bek++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Bek_") != NULL) {
        cnt_bek++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Bek/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Gelandang_") != NULL) {
        cnt_gelandang++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Gelandang/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      } else if (strstr(entry->d_name, "_Penyerang_") != NULL) {
        cnt_penyerang++;
        sprintf(dst_path, "/home/richard160421084/Documents/Sisop/Prak2/Shift/3/Penyerang/%s", entry->d_name);
        pid5 = fork();
        if (pid5 == 0) {
          execlp("mv", "mv", src_path, dst_path, NULL);
          exit(0);
        }
        waitpid(pid5, &status5, 0);
      }
    }
    closedir(dir);
  }
```

- Pindahkan pemain yang tersisa sesuai dengan posisinya masing-masing, hitung pula jumlah pemain di tiap posisinya selagi memindahkan mereka.
- File yang namanya memiliki substring "_Kiper_" dimasukkan ke folder Kiper
- File yang namanya memiliki substring "_Bek_" dimasukkan ke folder Bek
- File yang namanya memiliki substring "_Gelandang_" dimasukkan ke folder Gelandang
- File yang namanya memiliki substring "_Penyerang_" dimasukkan ke folder Penyerang
- Setelah selesai memindahkan semua pemain, tutup folder saat ini

```c
  for (int i=1; i<=cnt_bek; i++)
  {
    for (int j=1; j<=cnt_gelandang; j++)
    {
      int k = 10-i-j;
      if ((k > 0) && (k<=cnt_penyerang))
      {
        buatTim(i,j,k);
      }
    }
  }
```

- Lakukan looping untuk mendapatkan seluruh kemungkinan tim
- Diharuskan bahwa terdapat minimal 1 bek, 1 gelandang, dan 1 penyerang. Itu kenapa pada looping bek digunakan batasan 1 dan `cnt_bek`, serta pada looping gelandang digunakan batasan 1 dan `cnt_gelandang`. Kemudian karena tim terdiri dari 11 orang dan dapat dipastikan tepat ada 1 kiper maka tersisa 10 slot untuk posisi lain, maka jumlah pemain penyerang adalah `10-i-j` dimana `i` adalah jumlah bek yang terpilih, dan `j` adalah jumlah gelandang yang terpilih. Pastikan pula bahwa jumlah penyerang terletak diantara 1 dan `cnt_penyerang`

### Catatan Tambahan

Karena pada path kami menggunakan absolute path maka ubahlah path sesuai dengan user yang sedang digunakan

### Kendala

Tidak terdapat kendala dalam pengerjaan soal ini

# No 4

Soal ini meminta beberapa hal,

1. Membuat program c yang dapat menerima input seperti cronjob
2. Program c dapat mengeluarkan pesan error apabila input tidak sesuai
3. Program c dapat menjalankan suatu script bash
4. Program c berjalan pada background

Untuk menyelesaikan soal tersebut dilakukan langkah sebagai berikut

1. Buat file bernama `mainan.c` dengan menggunakan command `nano mainan.c`  
   ![buat file program](images/4_1.jpeg)
2. Tambahkan code berikut ke file `mainan.c`

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

int main(int argc, char *argv[])
{
  pid_t pid, sid, pid2;
  time_t current_time;
  struct tm *current_time_info;
  int hour, minute, seconds;
  char *path;

  if (argc != 5) {
    printf("Argumen: %s hour minute seconds path\n", argv[0]);
    return 1;
  }

  char *hour_arg = argv[1];
  char *minute_arg = argv[2];
  char *seconds_arg = argv[3];
  path = argv[4];

  if (hour_arg[0] != '*')
  {
    if (hour_arg[1] == '\0')
    {
      if ((hour_arg[0] < '0') || (hour_arg[0] > '9'))
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
    }
    else
    {
      if (hour_arg[2] != '\0')
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
      if ((hour_arg[0] == '0') || (hour_arg[0] == '1'))
      {
        if ((hour_arg[1] < '0') || (hour_arg[1] > '9'))
        {
          printf("Error: Argumen jam invalid.\n");
          return 1;
        }
      }
      if (hour_arg[0] == '2')
      {
        if ((hour_arg[1] < '0') || (hour_arg[1] > '3'))
        {
          printf("Error: Argumen jam invalid.\n");
          return 1;
        }
      }
      if ((hour_arg[0] < '0') || (hour_arg[0] > '2'))
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
    }
  }

  if (minute_arg[0] != '*')
  {
    if (minute_arg[1] == '\0')
    {
      if ((minute_arg[0] < '0') || (minute_arg[0] > '9'))
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
    }
    else
    {
      if (minute_arg[2] != '\0')
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
      if ((minute_arg[0] >= '0') && (minute_arg[0] <= '5'))
      {
        if ((minute_arg[1] < '0') || (minute_arg[1] > '9'))
        {
          printf("Error: Argumen menit invalid.\n");
          return 1;
        }
      }
      if ((minute_arg[0] < '0') || (minute_arg[0] > '5'))
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
    }
  }

  if (seconds_arg[0] != '*')
  {
    if (seconds_arg[1] == '\0')
    {
      if ((seconds_arg[0] < '0') || (seconds_arg[0] > '9'))
      {
        printf("Error: Argumen detik invalid.\n");
        return 1;
      }
    }
    else
    {
      if (seconds_arg[2] != '\0')
      {
        printf("Error: Argumen detik invalid.\n");
        return 1;
      }
      if ((seconds_arg[0] >= '1') && (seconds_arg[0] <= '5'))
      {
        if ((seconds_arg[1] < '0') || (seconds_arg[1] > '9'))
        {
          printf("Error: Argumen detik invalid.\n");
          return 1;
        }
      }
      if (seconds_arg[0] > '5')
      {
        printf("Error: Argumen detik invalid.\n");
        return 1;
      }
    }
  }


  if (hour_arg[0] == '*')
  {
    hour = -1;
  }
  else
  {
    hour = atoi(hour_arg);
  }

  if (minute_arg[0] == '*')
  {
    minute = -1;
  }
  else
  {
    minute = atoi(minute_arg);
  }

  if (seconds_arg[0] == '*')
  {
    seconds = -1;
  }
  else
  {
    seconds = atoi(seconds_arg);
  }

  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1)
  {
    time_t now = time(NULL);
    struct tm *tm_now = localtime(&now);

    if ((hour == -1 || tm_now->tm_hour == hour) && (minute == -1 || tm_now->tm_min == minute) && (seconds == -1 || tm_now->tm_sec == seconds))
    {
      pid2 = fork();
      if (pid2 == 0)
      {
        char *args[] = { "sh", path, NULL };
        execvp("sh", args);
        exit(0);
      }
    }
    sleep(1);
  }
}
```

2. Compile file program tadi dengan menggunakan command `gcc -o <hasil compile> <nama program>`, untuk soal ini digunakan nama compile `mainan`, maka gunakan command ``gcc -o mainan mainan.c```  
   ![Compile file program](images/4_2.jpeg)
3. Jalankan program dengan menggunakan command `./<hasil compile> <jam> <menit> <detik> <path ke script>`

### Hasil Program

a) Salah input  
![Salah input](images/4_3.jpeg)  
b) Salah jam  
![Salah jam](images/4_4.jpeg)  
c) Salah menit  
![Salah menit](images/4_5.jpeg)  
d) Salah detik  
![Salah detik](images/4_6.jpeg)  
e) Sukses  
![Sukses](images/4_7.jpeg)  
![ps aux](images/4_8.jpeg)

### Penjelasan code

- Pada hampir seluruh iterasi akan dilakukan forking sebab akan digunakan command exec(), apabila tidak dilakukan forking maka program akan berpindah ke program untuk menjalankan exec() dan tidak akan kembali ke program saat ini
- Digunakan proses daemon sebab program diinginkan untuk berjalan pada background

```c
  if (argc != 5) {
    printf("Argumen: %s hour minute seconds path\n", argv[0]);
    return 1;
  }
```

- `argc` merupakan jumlah argumen yang didapat
- Seharusnya terdapat 5 argumen yaitu program itu sendiri, jam, menit, detik, serta path ke script

```c
  char *hour_arg = argv[1];
  char *minute_arg = argv[2];
  char *seconds_arg = argv[3];
  path = argv[4];
```

Memindahkan isi input ke variabel

```c
  if (hour_arg[0] != '*')
  {
    if (hour_arg[1] == '\0')
    {
      if ((hour_arg[0] < '0') || (hour_arg[0] > '9'))
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
    }
    else
    {
      if (hour_arg[2] != '\0')
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
      if ((hour_arg[0] == '0') || (hour_arg[0] == '1'))
      {
        if ((hour_arg[1] < '0') || (hour_arg[1] > '9'))
        {
          printf("Error: Argumen jam invalid.\n");
          return 1;
        }
      }
      if (hour_arg[0] == '2')
      {
        if ((hour_arg[1] < '0') || (hour_arg[1] > '3'))
        {
          printf("Error: Argumen jam invalid.\n");
          return 1;
        }
      }
      if ((hour_arg[0] < '0') || (hour_arg[0] > '2'))
      {
        printf("Error: Argumen jam invalid.\n");
        return 1;
      }
    }
  }
```

- Melakukan pengecekan mengenai validasi jam yang diinputkan
- Apabila input jam adalah `*` maka tidak perlu dilakukan pengecekan
- Apabila input jam hanya 1 digit (`hour_arg[1] == '\0'`), cek apakah digit tersebut merupakan angka antara 0 hingga 9. Jika tidak, keluarkan error
- Apabila input jam terdiri dari lebih dari 2 digit (`hour_arg[2] != '\0'`), keluarkan error
- Apabila input jam ada 2 digit dan digit pertamanya 0 atau 1, maka cek apakah digit kedua merupakan angka antara 0 hingga 9. Jika tidak, keluarkan error
- Apabila input jam ada 2 digit dan digit pertamanya 2, maka cek apakah digit kedua merupakan angka antara 0 hingga 3. Jika tidak, keluarkan error
- Apabila input digit pertama jam diluar 0, 1, atau 2 maka keluarkan error

```c
  if (minute_arg[0] != '*')
  {
    if (minute_arg[1] == '\0')
    {
      if ((minute_arg[0] < '0') || (minute_arg[0] > '9'))
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
    }
    else
    {
      if (minute_arg[2] != '\0')
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
      if ((minute_arg[0] >= '0') && (minute_arg[0] <= '5'))
      {
        if ((minute_arg[1] < '0') || (minute_arg[1] > '9'))
        {
          printf("Error: Argumen menit invalid.\n");
          return 1;
        }
      }
      if ((minute_arg[0] < '0') || (minute_arg[0] > '5'))
      {
        printf("Error: Argumen menit invalid.\n");
        return 1;
      }
    }
  }
```

- Melakukan pengecekan mengenai validasi menit yang diinputkan
- Apabila input menit adalah `*` maka tidak perlu dilakukan pengecekan
- Apabila input menit hanya 1 digit (`hour_arg[1] == '\0'`), cek apakah digit tersebut merupakan angka antara 0 hingga 9. Jika tidak, keluarkan error
- Apabila input menit terdiri dari lebih dari 2 digit (`hour_arg[2] != '\0'`), keluarkan error
- Apabila input menit ada 2 digit dan digit pertamanya antara 0 hingga 5, maka cek apakah digit kedua merupakan angka antara 0 hingga 9. Jika tidak, keluarkan error
- Apabila input digit pertama menit diluar 0 hingga 5 maka keluarkan error

- Lakukan pengecekan yang sama untuk detik

```c
  if (hour_arg[0] == '*')
  {
    hour = -1;
  }
  else
  {
    hour = atoi(hour_arg);
  }

  if (minute_arg[0] == '*')
  {
    minute = -1;
  }
  else
  {
    minute = atoi(minute_arg);
  }

  if (seconds_arg[0] == '*')
  {
    seconds = -1;
  }
  else
  {
    seconds = atoi(seconds_arg);
  }
```

- Mengubah nilai input menjadi digit
- Jika input adalah `*`, maka anggap inputnya -1
- Jika input selain `*`, maka gunakan fungsi atoi() untuk mendapatkan angkanya

```c
  pid = fork();

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
```

Setup untuk menjalankan proses daemon

```c
  while (1)
  {
    time_t now = time(NULL);
    struct tm *tm_now = localtime(&now);

    if ((hour == -1 || tm_now->tm_hour == hour) && (minute == -1 || tm_now->tm_min == minute) && (seconds == -1 || tm_now->tm_sec == seconds))
    {
      pid2 = fork();
      if (pid2 == 0)
      {
        char *args[] = { "sh", path, NULL };
        execvp("sh", args);
        exit(0);
      }
    }
    sleep(1);
  }
```

- Dengan digunakan while(true) dan tidak ada break / return didalamnya maka looping akan berjalan terus pada background
- Dapatkan waktu saat ini, dan cek apakah waktu sekarang = waktu pada input atau waktu input adalah `*`. Apabila ya, maka buatlah proses baru untuk menjalankan script yang tertera pada input keempat (path)
- Sleep selama 1 detik untuk kemudian dicek kembali

### Keterangan Khusus

Pastikan bahwa script yang ditunjuk memiliki permission untuk dijalankan. Gunakan command `chmod +x [nama script]` apabila belum memiliki permission

### Kendala

Belum menemukan algoritma untuk mendapatkan waktu selanjutnya dimana shell perlu dieksekusi sehingga harus dilakukan pengecekan setiap detiknya
