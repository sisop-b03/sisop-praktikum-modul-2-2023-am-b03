#include<signal.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<syslog.h>
#include<string.h>
#include<time.h>
#include<wait.h>
#include<sys/prctl.h>
#include<stdbool.h> 

void createKiller(char* argv[], pid_t pid, pid_t sid);
void createFolder(char* folderName);
void downloadImage(char* folderName);
void zipFolder(char* folderName);
void removeFolder(char* folderName);

int main(int argc, char* argv[])
{
    // Check argument
    if(argc != 2 || (argv[1][1] != 'a' && argv[1][1] != 'b')) 
    {
        printf("Invalid Argument\n Usage: ./lukisan -[a/b]\n");
        exit(0);
    }
    
    // Daemonize
    pid_t pid, sid;

    pid = fork();
    if(pid < 0) exit(EXIT_FAILURE);
    if(pid > 0) exit(EXIT_SUCCESS);

    umask(0);

    sid = setsid();
    if(sid < 0) exit(EXIT_FAILURE);

    createKiller(argv, pid, sid);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1)
    {
        // Create folder
        pid_t child;
        char folderName[30];
        time_t currTime = time(NULL);
        struct tm* localTime = localtime(&currTime);
        strftime(folderName, 30, "%Y-%m-%d_%H:%M:%S", localTime);

        createFolder(folderName);

        // Download image
        child = fork();
        if(child == 0)
        {
            downloadImage(folderName);
            zipFolder(folderName);
            removeFolder(folderName);
        }
        sleep(30);
    }
}

void createKiller(char *argv[], pid_t pid, pid_t sid)
{
    FILE *fileptr = fopen("killer.c", "w");

    // Killer.c
    char input[1024] = ""
    "#include <unistd.h>\n"
    "#include <wait.h>\n"
    "int main() {\n"
        "pid_t child = fork();\n"
        "if (child == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "while(wait(NULL) > 0);\n"
        "char *argv[] = {\"rm\", \"killer\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"
    "}\n";

    // Mode A
    char command[1024];
    if (strcmp(argv[1], "-a") == 0) {
        sprintf(command, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
        fprintf(fileptr, input, command, "/usr/bin/pkill");
    }

    // Mode B
    if (strcmp(argv[1], "-b") == 0) {
        sprintf(command, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
        fprintf(fileptr, input, command, "/bin/kill");
    }

    fclose(fileptr);

    // Compile killer.c
    pid = fork();
    if(pid == 0)
    {    
        char *command[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execv("/usr/bin/gcc", command);
    }
    while(wait(NULL) != pid);

    // Remove killer.c
    pid = fork();
    if (pid == 0) {
    	char *argv[] = {"rm", "killer.c", NULL};
    	execv("/bin/rm", argv);
    }
    while(wait(NULL) != pid);
}

void createFolder(char* folderName)
{
    pid_t child;
    child = fork();
    if(child == 0)
    {
        char *command[] = {"mkdir", folderName, NULL};
        execv("/bin/mkdir", command);
    }
    while(wait(NULL) != child);
}

void downloadImage(char* folderName)
{
    pid_t child;
    char imgName[30], location[50], link[50];

    for(int i = 0; i < 15; i++)
    {
        child = fork();
        if(child == 0)
        {
            time_t t = time(NULL);
            struct tm* localTime = localtime(&t);
            strftime(imgName, 30, "%Y-%m-%d_%H:%M:%S.png", localTime);
            sprintf(location, "%s/%s", folderName, imgName);
            sprintf(link, "https://picsum.photos/%ld", (t % 1000) + 50);

            char *command[] = {"wget", "-q", "-O", location, link, NULL};
            execv("/usr/bin/wget", command);
        }
        sleep(5);
    }
    while(wait(NULL) > 0);
}

void zipFolder(char* folderName)
{
    char zipName[30];
    sprintf(zipName, "%s.zip", folderName);
    
    pid_t child;
    child = fork();
    if(child == 0)
    {
        char *command[] = {"zip", "-r", zipName, folderName, NULL};
        execv("/usr/bin/zip", command);
    }
    while(wait(NULL) != child);
}

void removeFolder(char* folderName)
{
    char *command[] = {"rm", "-r", folderName, NULL};
    execv("/bin/rm", command);
}
